<?php
namespace App;

use App\Entity\Template;
use App\Service\Template\TemplateFormatterService;

class TemplateManager
{

    public function getTemplateComputed(Template $template, array $data)
    {
        if (!$template) {
            throw new \RuntimeException('no tpl given');
        }

        $replacedTemplate = clone($template);

        $newSubject = new TemplateFormatterService($replacedTemplate->subject, $data);
        $newContent = new TemplateFormatterService($replacedTemplate->content, $data);

        $replacedTemplate->subject = $newSubject->editTemplate();
        $replacedTemplate->content = $newContent->editTemplate();

        return $replacedTemplate;
    }
}
