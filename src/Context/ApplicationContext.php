<?php

namespace App\Context;

use App\Entity\Learner;
use App\Helper\SingletonTrait;

class ApplicationContext
{
    use SingletonTrait;

    /**
     * @var Learner
     */
    private Learner $currentLearner;

    protected function __construct()
    {
    }

    /**
     * @return Learner
     */
    public function getCurrentLearner(): Learner
    {
        return $this->currentLearner;
    }

    /**
     * @param Learner $currentLearner
     */
    public function setCurrentLearner(Learner $currentLearner): void
    {
        $this->currentLearner = $currentLearner;
    }
}
