<?php

namespace App\Service;

class RenderService
{

    public function __construct() {
    }

    public function render(string $text):string
    {
        return (string) $text;
    }
    public function renderHTML(string $text):string
    {
        return (string) '<p>' . $text . '</p>';
    }

}
