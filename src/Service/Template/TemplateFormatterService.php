<?php

namespace App\Service\Template;

use App\Entity\Lesson;
use App\Context\ApplicationContext;
use App\Helper\TextOperationTrait;
use App\Service\Render\RenderService;

use App\Repository\InstructorRepository;
use App\Repository\LessonRepository;
use App\Repository\MeetingPointRepository;

class TemplateFormatterService
{
    use TextOperationTrait;

    private $text;

    private $lesson;

    private $learner;

    public function __construct($text, array $data)
	{

        $applicationContext = ApplicationContext::getInstance();

		$this->text = $text;
        $this->lesson = (isset($data['lesson']) and $data['lesson'] instanceof Lesson) ? $data['lesson'] : null;
        $this->learner = (isset($data['learner'])  and ($data['learner']  instanceof Learner))  ? $data['learner']  : $applicationContext->getCurrentLearner();

	}

    public function editTemplate()
    {
        if ($this->lesson)
        {
            $meetingPoint = MeetingPointRepository::getInstance()->getById($this->lesson->meetingPointId);
            $instructor = InstructorRepository::getInstance()->getById($this->lesson->instructorId);

            $this->stringReplace('[lesson:instructor_link]', 'instructors/' . $instructor->id .'-'.urlencode($instructor->firstname), $this->text);

            if(strpos($this->text, '[lesson:summary_html]')  !== false) {
                $this->stringReplace('[lesson:summary_html]', RenderService::render($this->lesson->id), $this->text);
            }

            if(strpos($this->text, '[lesson:summary]')  !== false) {
                $this->stringReplace('[lesson:summary]', RenderService::renderHTML($this->lesson->id), $this->text);
            }
            
            if($instructor) {
                $this->stringReplace('[lesson:instructor_name]', $instructor->firstname, $this->text);
                $this->stringReplace('[instructor_link]', 'instructors/' .$instructor->id .'-'.urlencode($instructor->firstname), $this->text);
            } else {
                $this->stringReplace('[instructor_link]', '', $this->text);
            }

            if($meetingPoint) {
                $this->stringReplace('[lesson:meeting_point]', $meetingPoint->name, $this->text);
            }

            $this->stringReplace('[lesson:start_date]', $this->lesson->start_time->format('d/m/Y'), $this->text);
            $this->stringReplace('[lesson:start_time]', $this->lesson->start_time->format('H:i'), $this->text);

            $this->stringReplace('[lesson:end_time]', $this->lesson->end_time->format('H:i'), $this->text);
        }

        if($this->learner) {
            $this->stringReplace('[user:first_name]', ucfirst(strtolower($this->learner->firstname)), $this->text);
        }

        return $this->text;
    }
}
