<?php

namespace App\Helper;

trait TextOperationTrait
{
    public function stringReplace($search, $replace, &$text)
    {
        if(strpos($text, $search) !== false){
            $text = str_replace($search,  $replace, $text);
        }

        return $text;
    }
}
